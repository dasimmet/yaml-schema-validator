# YAML Schema Validator

This is a [Sphinx Gitlab Pages Page](https://dasimmet.gitlab.io/yaml-schema-validator/) to validate a given YAML instance against its [json-schema.org](https://json-schema.org) using Browser based Javascript and the [AJV validator](https://ajv.js.org/)

Its possible to store the application state inside the URL hash using LZString compression, as long as the browser supports an URL with this length (at least 2083 characters on MS Edge).