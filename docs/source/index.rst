.. yaml-schema-validator documentation master file, created by
   sphinx-quickstart on Sat May  7 10:59:53 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

yaml-schema-validator
=====================

.. toctree::
   :maxdepth: 2
   :hidden:

   example
   state
   oauthresponse

This page validates a given yaml/json instance against
its `json-schema.org <http://json-schema.org/>`_ using the `AJV validator <https://ajv.js.org/>`_.

`Source Code on Gitlab.com <https://gitlab.com/dasimmet/yaml-schema-validator/-/tree/main>`_

.. raw:: html

  <script src="https://cdnjs.cloudflare.com/ajax/libs/ajv/8.11.0/ajv2020.bundle.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/lz-string/1.4.4/lz-string.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/js-yaml/4.1.0/js-yaml.min.js" integrity="sha512-CSBhVREyzHAjAFfBlIBakjoRUKp5h7VSweP0InR/pAJyptH7peuhCsqAI/snV+TwZmXZqoUklpXp6R6wMnYf5Q==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
  <script src="_static/validator.js"></script>
  <link rel="stylesheet" href="_static/validator.css">


Schema
------

.. raw:: html

  <textarea id="schemainput" name="schemainput" oninput="validate()" cols="40" rows="5" spellcheck="false"></textarea>

Instance
--------

.. raw:: html

  <textarea id="instanceinput" name="instanceinput" oninput="validate()" cols="40" rows="5" spellcheck="false"></textarea>

Result
------

.. raw:: html

  <textarea id="resultArea" name="resultArea" readonly="readonly" spellcheck="false"></textarea>
  <button onclick="permalink()">Save Input in URL</button>



.. jsonschema:: _static/example.instance.yaml