function validate(){
    const ajvInstance = new ajv2020();
    const schemainput = document.getElementById("schemainput");
    const instanceinput = document.getElementById("instanceinput");
    sessionStorage.setItem('schema', schemainput.value);
    const result = document.getElementById("resultArea");
    FitToContent(schemainput, 500);
    FitToContent(instanceinput, 500);
    sessionStorage.setItem('schema', schemainput.value);
    sessionStorage.setItem('instance', instanceinput.value);

    var resultObj = false;
    var schema = null;
    var instance = null;
    var validate = null;

    try {
        schema = jsyaml.load(schemainput.value);
        validate = ajvInstance.compile(schema);
        schemainput.style.backgroundColor = null;
    } catch (error) {
        result.value = error.message;
        schemainput.style.backgroundColor = "red";
    }

    try {
        instance = jsyaml.load(instanceinput.value);
        instanceinput.style.backgroundColor = null;
    } catch (error) {
        instanceinput.style.backgroundColor = "red";
        result.value = error.message;
    }

    if (schema != null && instance != null && validate != null){
        try {
            resultObj = validate(instance);
            result.value = resultObj ? "valid" : jsyaml.dump(validate.errors);
        } catch (error) {
            console.error(error);
            result.value = error.message;
        }
    }

    result.style.backgroundColor = resultObj ? "green" : "red";
    FitToContent(result, 500);
}
function permalink(){
    const schemainput = document.getElementById("schemainput");
    const instanceinput = document.getElementById("instanceinput");
    const compressed = LZString.compressToEncodedURIComponent(JSON.stringify({
        "schema": schemainput.value,
        "instance": instanceinput.value
    }));
    const parsedUrl = new URL(window.location.href);
    parsedUrl.pathname = parsedUrl.pathname.replace(/\/index\.html.*/,"/")
    parsedUrl.hash = "#state:" + compressed;
    window.prompt("Permalink (" + parsedUrl.href.length + " characters):", parsedUrl.href);
}

function FitToContent(id, maxHeight)
{
    var text = id && id.style ? id : document.getElementById(id);
    if ( !text )
        return;

    text.style.height = "auto";
    text.style.height = Math.min(text.scrollHeight,maxHeight) + "px";
}

function tabTextArea(id, indent=2){
    var textArea = id && id.style ? id : document.getElementById(id);

    textArea.addEventListener('keydown', function(e) {
        if (e.key == 'Tab' && !e.getModifierState("Control")) { 
            e.preventDefault();
            var start = this.selectionStart;
            var end = this.selectionEnd;
            const firstLineStart = this.value.lastIndexOf('\n', start - 1) + 1;
            var nindent = indent;

            if (e.getModifierState("Shift")) {
                nindent = -indent;
                for (var i=0; i<indent; i++){
                    if (this.value[firstLineStart+i] != " "){
                        nindent = -i;
                        break;
                    }
                }

                this.value = this.value.substring(0, firstLineStart) + 
                    this.value.substring(firstLineStart-nindent);

            } else {
                // set textarea value to: text before caret + tab + text after caret
                this.value = this.value.substring(0, firstLineStart) +
                    " ".repeat(nindent) +
                    this.value.substring(firstLineStart);
            
            }
            // put caret at right position again
            this.selectionStart =
                this.selectionEnd = Math.max(firstLineStart, start + nindent);
      
            const event = new Event('input');
            this.dispatchEvent(event);
        }
      });

}
async function fetchText(res){
    const a = await fetch(res);
    return a.text();
}

function INSTANCE_EXAMPLE(){
    return fetchText("_static/example.instance.yaml");
}

function SCHEMA_EXAMPLE(){
    return fetchText("_static/example.schema.yaml");
}

function loadHash(){
    const schemainput = document.getElementById("schemainput");
    const instanceinput = document.getElementById("instanceinput");

    const state = JSON.parse(LZString.decompressFromEncodedURIComponent(window.location.hash.replace(/^#state:/,"")));
    schemainput.value = state['schema'];
    instanceinput.value = state['instance'];
}

addEventListener('hashchange', ()=>{try{loadHash();}catch{}});

async function loadedHook() {
    tabTextArea("schemainput");
    tabTextArea("instanceinput");
    const schemainput = document.getElementById("schemainput");
    const instanceinput = document.getElementById("instanceinput");

    try {
        loadHash();
    } catch (error){
        const schema = sessionStorage.getItem('schema');
        if (schema != undefined) {
            schemainput.value = schema
        } else {
            schemainput.value = await SCHEMA_EXAMPLE();
        }
        const instance = sessionStorage.getItem('instance');
        if (instance != undefined) {
            instanceinput.value = instance;
        } else {
            instanceinput.value = await INSTANCE_EXAMPLE();
        }
    }
    schemainput.focus();


    validate();
}

document.addEventListener("DOMContentLoaded", loadedHook);