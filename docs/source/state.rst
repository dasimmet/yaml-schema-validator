Application State
=================

the application state can be passed to the main page using the following syntax:

`<https://dasimmet.gitlab.io/yaml-schema-validator/#state:LZSTRING>`_

where ``LZSTRING`` should be replaced with a `LZString.compressToEncodedURIComponent <https://pieroxy.net/blog/pages/lz-string/index.html>`_
compressed JSON object of the following structure:

.. code-block:: json

    {
        "schema": "SCHEMA_STRING",
        "instance": "INSTANCE_STRING"
    }

when following such a Link ``SCHEMA_STRING`` and ``INSTANCE_STRING`` will be inserted
into the respective boxes instead of the example.

LZString can for example be included into webpages from ``cdnjs`` and executed in Browsers:

.. code-block:: html

    <script src="https://cdnjs.cloudflare.com/ajax/libs/lz-string/1.4.4/lz-string.min.js"></script>
    <script type="text/javascript">
        var hash = "#state:" + LZString.compressToEncodedURIComponent(JSON.stringify({
            "schema": schemainput.value,
            "instance": instanceinput.value
        }));
    </script>