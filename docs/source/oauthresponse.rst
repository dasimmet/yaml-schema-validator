
oauthresponse
=============

.. raw:: html


  <script>
    var oauth = {
    }
    window.addEventListener('load', async (event) => {

        oauth.metadata_url = document.getElementById("metadata_url");
        oauth.code = document.getElementById("oauthcode");
        oauth.uri = new URL(window.location.href);
        oauth.code.value = oauth.uri.searchParams.get("code");
        oauth.metadata_url.value = oauth.uri.searchParams.get("metadata_url");
        console.log(oauth);
        
        oauth.metadata_url.addEventListener('input', getOauthresponse);
        await getOauthresponse();
    });
    async function getOauthresponse(){
        oauth.uri.searchParams.set("metadata_url", oauth.metadata_url.value);
        window.history.replaceState({page: 0}, "title 3", oauth.uri.href);
        resp = await fetch(oauth.metadata_url.value);
        oauth.metadata = await resp.json();

        
        console.log(oauth.metadata);
        var auth_uri = new URL(oauth.metadata.authorization_endpoint);
        auth_uri.searchParams.set("code", oauth.code.value);
        resp = await fetch(auth_uri.href);
        console.log(resp);
    }
    async function copyToClipboard(){
        oauth.code.select();
        oauth.code.setSelectionRange(0, 99999);
        // navigator.clipboard.writeText(oauth.code.value);
        document.execCommand("copy");
    }
  </script>
  <p><input id="metadata_url" style="width: 100%;display: none;"></input></p>
  <p>Please return this code to your Application:</p>
  <p style="width: 100%;">
    <input id="oauthcode" ></input>
    <button class="btn btn-neutral float-right" onclick="copyToClipboard()">Copy to Clipboard</button>
  </p>