Schema Documentation
====================

the default schema and instance are shown by
`sphinx-jsonschema <https://sphinx-jsonschema.readthedocs.io>`_ here:

Example Schema
--------------

.. jsonschema:: _static/example.schema.yaml
    :lift_definitions:


Example Instance
----------------

.. literalinclude:: _static/example.instance.yaml
   :language: yaml